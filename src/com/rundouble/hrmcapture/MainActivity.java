package com.rundouble.hrmcapture;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.UUID;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends Activity implements OnItemClickListener
{

    private BluetoothAdapter adaptor;
    private static final int BT_REQUEST = 501;
    private boolean discovering = false;
    private ArrayAdapter<DeviceWrapper> listAdapter;

    private final UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private BluetoothSocket btSocket;
    private StringBuilder builder = new StringBuilder();
    private ProgressDialog prog;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listAdapter = new ArrayAdapter<MainActivity.DeviceWrapper>(this, android.R.layout.simple_list_item_1);
        ListView lv = (ListView) findViewById(R.id.listView1);
        lv.setOnItemClickListener(this);
        lv.setAdapter(listAdapter);
        initBT();
    }

    // @Override
    // public boolean onCreateOptionsMenu(Menu menu)
    // {
    // // Inflate the menu; this adds items to the action bar if it is present.
    // getMenuInflater().inflate(R.menu.activity_main, menu);
    // return true;
    // }

    private void initBT()
    {
        adaptor = BluetoothAdapter.getDefaultAdapter();
        if (adaptor == null)
        {
            return;
        }
        if (adaptor.isEnabled())
        {
            findDevices();
        }
        else
        {
            Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(intent, BT_REQUEST);
        }
    }

    private void findDevices()
    {
        for (BluetoothDevice dev : adaptor.getBondedDevices())
        {
            listAdapter.add(new DeviceWrapper(dev));
            listAdapter.notifyDataSetChanged();
        }
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        registerReceiver(mReceiver, filter); // Don't forget to unregister
                                             // during onDestroy
        discovering = true;
        adaptor.startDiscovery();
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            String action = intent.getAction();
            // When discovery finds a device
            if (BluetoothDevice.ACTION_FOUND.equals(action))
            {
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                listAdapter.add(new DeviceWrapper(device));
                listAdapter.notifyDataSetChanged();
            }
        }

    };

    public void stopDiscovery()
    {
        if (discovering)
        {
            adaptor.cancelDiscovery();
            unregisterReceiver(mReceiver);
            discovering = false;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == BT_REQUEST)
        {
            findDevices();
        }
    }

    class DeviceWrapper
    {

        BluetoothDevice device;

        public DeviceWrapper(BluetoothDevice device)
        {
            this.device = device;
        }

        public BluetoothDevice getDevice()
        {
            return device;
        }

        @Override
        public String toString()
        {
            return device.getName();
        }
    }

    private void connect(BluetoothDevice device)
    {
        try
        {
            btSocket = device.createRfcommSocketToServiceRecord(uuid);
            btSocket.connect();
            manageConnection();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    private void logdata(byte[] buffer, int len)
    {
        builder.append("" + new Date());
        builder.append(": (hex)");
        for (int i = 0; i < len; i++)
        {
            builder.append(Integer.toHexString(buffer[i]));
            builder.append(" ");
        }
        builder.append("\n");
        builder.append("" + new Date());
        builder.append(": (dec) ");
        for (int i = 0; i < len; i++)
        {
            builder.append(Integer.toString(buffer[i]));
            builder.append(" ");
        }
        builder.append("\n");
    }

    private void manageConnection()
    {
        try
        {
            final InputStream inputStream = btSocket.getInputStream();
            byte[] buffer = new byte[2000];
            for (int i = 0; i < 20; i++)
            {
                final int j = i;
                runOnUiThread(new Runnable() {

                    @Override
                    public void run()
                    {
                        prog.setMessage("Collecting item: " + j + " of 20");
                    }
                });
                int len = inputStream.read(buffer);
                Log.i("READING", "Reading: " + i + " len: " + len);
                logdata(buffer, len);
            }
            Log.i("READING", builder.toString());
            runOnUiThread(new Runnable() {

                @Override
                public void run()
                {
                    prog.cancel();
                    try
                    {
                        sendEmail();
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                }
            });
        }
        catch (Exception ex)
        {
            builder.append("Error " + ex.getMessage());
        }
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3)
    {
        prog = new ProgressDialog(this);
        prog.setMessage("Collecting data");
        prog.show();
        stopDiscovery();
        final DeviceWrapper item = listAdapter.getItem(position);
        final BluetoothDevice device = item.getDevice();
        builder.append("Name [" + device.getName() + "]\n");
        new Thread(new Runnable() {

            @Override
            public void run()
            {
                connect(device);
            }
        }).start();
    }

    private void sendEmail() throws IOException
    {
        Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:help@rundouble.com"));

        intent.putExtra(Intent.EXTRA_SUBJECT, "HRM log");
        intent.putExtra(Intent.EXTRA_TEXT, builder.toString());

        startActivity(intent);
    }

}
